import { combineReducers } from "redux";
import sampleReducer from "./sample_reducer";

const rootReducer = combineReducers({
  // object to be reduced: valueFromReducer
  sample_reducer: sampleReducer
});

export default rootReducer;
