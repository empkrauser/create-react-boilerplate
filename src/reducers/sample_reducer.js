export default function(state = null, action) {
  switch (action.type) {
    case "ACTION_NAME":
      return action.payload;
  }

  return state;
}
