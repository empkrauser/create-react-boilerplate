import React, { Component } from "react";
import Header from "./components/header";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div className="main-content">
          <div className="grid-container">{this.props.children}</div>
        </div>
        <footer />
      </div>
    );
  }
}

export default App;
