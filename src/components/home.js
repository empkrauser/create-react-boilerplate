import React, { Component } from "react";

class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h1>Welcome to Home</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Praesentium repudiandae dolor dignissimos fugiat eius, alias
                porro adipisci, enim sequi voluptates eveniet debitis
                voluptatibus aperiam quod vel dolorem a minima neque. Lorem
                ipsum dolor sit amet, consectetur adipisicing elit. Praesentium
                repudiandae dolor dignissimos fugiat eius, alias porro adipisci,
                enim sequi voluptates eveniet debitis voluptatibus aperiam quod
                vel dolorem a minima neque. Lorem ipsum dolor sit amet,
                consectetur adipisicing elit. Praesentium repudiandae dolor
                dignissimos fugiat eius, alias porro adipisci, enim sequi
                voluptates eveniet debitis voluptatibus aperiam quod vel dolorem
                a minima neque.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Praesentium repudiandae dolor dignissimos fugiat eius, alias
                porro adipisci, enim sequi voluptates eveniet debitis
                voluptatibus aperiam quod vel dolorem a minima neque. Lorem
                ipsum dolor sit amet, consectetur adipisicing elit. Praesentium
                repudiandae dolor dignissimos fugiat eius, alias porro adipisci,
                enim sequi voluptates eveniet debitis voluptatibus aperiam quod
                vel dolorem a minima neque. Lorem ipsum dolor sit amet,
                consectetur adipisicing elit. Praesentium repudiandae dolor
                dignissimos fugiat eius, alias porro adipisci, enim sequi
                voluptates eveniet debitis voluptatibus aperiam quod vel dolorem
                a minima neque.
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Home;
